#!/usr/bin/ruby
require 'getoptlong'

def parse_args
  unless ARGV.length >= 1 and ARGV.length <= 5
    puts 'Usage: --file=<path> --code --shift-amount=<shift amount>'
    exit 1
  end

  file = ''
  code = false
  amount = 8
args = ARGV
args.size.times do |i|
  if args[i-1] == '--file'
    file = args[i]
  end
  if args[i-1] =~ /--shift-amount=*/
    a = args[i-1].index('=')
    amount = args[i-1][a+1, args[i-1].length-1]
  end
  if args[i-1] == '--code'
    code = true
  end
end
if file == ''
    puts 'Usage: --file=<path> --code --shift-amount=<shift amount>'
    exit 1
end 
  return [file, amount, code]
end
class Cipher

  def initialize(amount, code)
    @amount = amount.to_i
      @code = code
  end

  def cipher
    alphabet.zip(shifted_alphabet).to_h
  end

  def key
    cipher
  end

  private

  def alphabet
    ('a'..'z').to_a.append(('A'..'Z').to_a).flatten
  end

  def shifted_alphabet
    shifted = alphabet
    xtimes = @code ? @amount : 10 + @amount
    xtime.times { shifted.map(&:next!) }
    shifted = shifted.map { |elem| if elem.length > 1 then elem[-1] else elem end }
    end
  end
class Message
  def initialize(message, cipher)
    @message = message
    @cipher = cipher
  end

  def encrypt
    temp = ""
    @message.each_char do |char|
      if @cipher.cipher[char]
        temp << @cipher.cipher[char]
      else
        temp << char
      end
    end
    return temp
  end
  def decrypt
    temp = ""
    @message.each_char do |char|
      if @cipher.cipher[char]
        temp << @cipher.cipher[char]
      else
        temp << char
      end
    end
    return temp
  end

  def to_s
    @message
  end
end
if $PROGRAM_NAME == __FILE__
temp = parse_args
file = temp[0]
amount = temp[1]
code = temp[2]
cipher = Cipher.new(amount, code)
  
message = File.read(file)
  
puts Message.new(message, cipher).decrypt
end
